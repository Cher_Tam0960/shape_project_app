/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.shape_project_app;

/**
 *
 * @author kitti
 */
public class Triangle extends Shape {

    private double width;
    private double high;

    public Triangle(double width, double high) {
        super("Triangle");
        this.width = width;
        this.high = high;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    @Override
    public double calArea() {
        return 0.5*width*high;
    }

    @Override
    public double calPerimater() {
        return width*3;
    }

}
