/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.shape_project_app;

/**
 *
 * @author kitti
 */
public class Square extends Shape {

    private double width;

    public Square(double width) {
        super("Square");
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public double calArea() {
        return width * width;
    }
    @Override
    public double calPerimater() {
        return width * 4;
    }

    public void setSide(double side) {
        this.width = side;
    }
    
}
