/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.shape_project_app;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author kitti
 */
public class SquareFrame extends JFrame {

    JLabel lblwidth;
    JTextField txtwide;
    JButton btnCalculate;
    JLabel lblResult;

    public SquareFrame() {
        super("Square");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblwidth = new JLabel("width : ", JLabel.TRAILING);
        lblwidth.setSize(50, 20);
        lblwidth.setLocation(5, 5);
        lblwidth.setBackground(Color.WHITE);
        lblwidth.setOpaque(true);
        this.add(lblwidth);

        txtwide = new JTextField();
        txtwide.setSize(50, 20);
        txtwide.setLocation(60, 5);
        this.add(txtwide);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Square width = ??? area = ??? parameter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.GRAY);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    System.out.println("Clicked!!!");
                    String strRadius = txtwide.getText();
                    double strWidth = Double.parseDouble(strRadius);
                    Square square = new Square(strWidth);
                    lblResult.setText("Square  = " + String.format("%.2f", square.getWidth())
                            + " area = " + String.format("%.2f", square.calArea())
                            + " parimeter = " + String.format("%.2f", square.calPerimater()));
                } catch (Exception ex) {
                    System.out.println("Error!!!" + ex.getMessage());
                    System.out.println(ex.getCause());
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtwide.setText("");
                    txtwide.requestFocus();

                }

            }

        });

    }

    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}
