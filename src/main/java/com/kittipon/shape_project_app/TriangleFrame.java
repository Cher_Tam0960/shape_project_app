/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.shape_project_app;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author kitti
 */
public class TriangleFrame extends JFrame {

    JLabel lblwidth;
    JTextField txtwidth;
    JLabel lblhigh;
    JTextField txthigh;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblwidth = new JLabel("width : ", JLabel.TRAILING);
        lblwidth.setSize(50, 20);
        lblwidth.setLocation(5, 5);
        lblwidth.setBackground(Color.WHITE);
        lblwidth.setOpaque(true);
        this.add(lblwidth);

        txtwidth = new JTextField();
        txtwidth.setSize(50, 20);
        txtwidth.setLocation(60, 5);
        this.add(txtwidth);

        lblhigh = new JLabel("high : ", JLabel.TRAILING);
        lblhigh.setSize(50, 20);
        lblhigh.setLocation(5, 25);
        lblhigh.setBackground(Color.WHITE);
        lblhigh.setOpaque(true);
        this.add(lblhigh);

        txthigh = new JTextField();
        txthigh.setSize(50, 20);
        txthigh.setLocation(60, 25);
        this.add(txthigh);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(60, 60);
        this.add(btnCalculate);

        lblResult = new JLabel("<html>TRIANGLE <br/>width = ??? <br/>high = ??? "
                + "<br/>area = ??? <br/>parimeter = ???</html>");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 90);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.GRAY);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    System.out.println("Clicked!!!");
                    String strwidth = txtwidth.getText();
                    String strhigh = txthigh.getText();
                    double width = Double.parseDouble(strwidth); 
                    double high = Double.parseDouble(strhigh);
                    Triangle triangle = new Triangle(width,high);
                    lblResult.setText("<html>TRIANGLE" + String.format("<br/> width = %.2f", triangle.getWidth())
                            +String.format("<br/>high = _%.2f", triangle.getHigh())
                            + "<br/>area = " + String.format("%.2f", triangle.calArea())
                            + "<br/>parimeter = " + String.format("%.2f</html>", triangle.calPerimater()));
                } catch (Exception ex) {
                    System.out.println("Error!!!" + ex.getMessage());
                    System.out.println(ex.getCause());
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtwidth.setText("");
                    txthigh.setText("");
                    txtwidth.requestFocus();

                }

            }

        });
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
